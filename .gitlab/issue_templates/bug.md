## Quick Information
<!-- This is to help replicate the issue as closeley as possible !-->
- **Kubernetes distribution:** GKE / EKS / AKS / K3D / Minikube / Other <!-- Delete as appropriate !-->
- **Kustomize-dandelion branch version:** Master / v11 / fork-branch <!-- Delete as appropriate !-->
<!-- Indicate whether any changes have been made to Readme instructions of kustomize files !-->

## What Happened?
<!-- A brief description of what happened when you tried to perform an action !-->

## Expected result
<!-- What should have happened when you performed the actions !-->

## Steps to reproduce
<!-- List the steps required to produce the error. These should be as few as possible !-->

## Screenshots
<!-- Any relevant screenshots which show the issue !-->

## Priority/Severity
<!-- Delete as appropriate. The priority and severity assigned may be different to this !-->
- High (anything that impacts cluster services or the normal deployment flow)
- Medium (anything that negatively affects the user experience)
- Low (anything else e.g., typos, bad practices, etc.)

/label ~bug